<!DOCTYPE html>
<html lang="en">
<title><?=$title?></title>
<head>
    <title><?=$title?></title>
    <style> 
    .invalid-feedback:empty {
        display: none;
    }
    .invalid-feedback {
        font-size: smaller;
        color: rgb(153, 16, 16);
    }
</style>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>

<body>
<div class="container-fluid" style="width: 800px;margin-top:50px;">
<?php if($this->session->flashdata('message')): ?>
    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('message') ?></div>
	<?php endif ?>

    <div class="table-responsive">
        <div>
            <a href="<?php echo base_url('mahasiswa/add'); ?>">Tambah data</a>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>No. handphone</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($mahasiswa as $mhs): ?>
                <tr>
                    <td><?=$mhs->NIM?></td>
                    <td><?=$mhs->Nama?></td>
                    <td><?=$mhs->email?></td>
                    <td><?=$mhs->hp?></td>
                    <td><?=$mhs->alamat?></td>
                    <td><a href="<?php echo base_url('mahasiswa/edit/'.$mhs->id); ?>">edit</a> || <a href="<?php echo base_url('mahasiswa/delete/'.$mhs->id); ?>" onclick="return confirm('yakin hapus data ?')">delete</a></td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
</body>

</html>