<!DOCTYPE html>
<html lang="en">

<head>
    <title><?=$title?></title>
    <style> 
    .invalid-feedback:empty {
        display: none;
    }
    .invalid-feedback {
        font-size: smaller;
        color: rgb(153, 16, 16);
    }
</style>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>

<body>
<div class="container-fluid" style="width: 500px;margin-top:50px;">
    
    <form method="post" name="add_mahasiswa" action="">
    <div class="form-group">
        <label>NIM</label>
        <input type="text" name="nim" class="form-control" value="<?=$mahasiswa->NIM?>">
        <div class="invalid-feedback"><?= form_error('nim') ?></div>
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" class="form-control" value="<?=$mahasiswa->Nama?>">
        <div class="invalid-feedback"><?= form_error('nama') ?></div>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" value="<?=$mahasiswa->email?>">
        <div class="invalid-feedback"><?= form_error('email') ?></div>
    </div>
    <div class="form-group">
        <label >hp</label>
        <input type="text" name="hp" class="form-control" value="<?=$mahasiswa->hp?>">
        <div class="invalid-feedback"><?= form_error('hp') ?></div>
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" name="alamat" class="form-control" value="<?=$mahasiswa->alamat?>">
        <div class="invalid-feedback"><?= form_error('alamat') ?></div>
    </div>
    <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>
</body>

</html>