<?php

class M_mahasiswa extends CI_Model
{
	private $_table = "mahasiswa";

	public function rules()
	{
		return [
            [
				'field' => 'nim', 
				'label' => 'nim', 
				'rules' => 'required|min_length[8]|max_length[8]'
			],
			[
				'field' => 'nama', 
				'label' => 'Name', 
				'rules' => 'required'
			],
			[
				'field' => 'email', 
				'label' => 'Email', 
				'rules' => 'required|valid_email|max_length[32]'
			],
            [
				'field' => 'email', 
				'label' => 'Email', 
				'rules' => 'required|valid_email|max_length[32]'
			],
			[
				'field' => 'hp', 
				'label' => 'hp', 
				'rules' => 'required'
            ],
		];
	}

    public function get()
	{
        $query = $this->db->get($this->_table);
		return $query->result();
	}

	public function insert($mahasiswa)
	{
		if(!$mahasiswa){
			return;
		}

		return $this->db->insert($this->_table, $mahasiswa);
	}

    public function find($id)
	{
		if (!$id) {
			return;
		}

		$query = $this->db->get_where($this->_table, array('id' => $id));
		return $query->row();
	}

    public function update($mahasiswa)
	{
		if (!isset($mahasiswa['id'])) {
			return;
		}

		return $this->db->update($this->_table, $mahasiswa, ['id' => $mahasiswa['id']]);
	}

    public function delete($id)
    {
        if (!$id) {
			return;
		}

		return $this->db->delete($this->_table, ['id' => $id]);
    }
}