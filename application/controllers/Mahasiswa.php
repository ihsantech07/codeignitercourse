<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_mahasiswa');
	}

	public function index()
	{
		$data['title'] = "Data Mahasiswa";
		$data['mahasiswa'] = $this->m_mahasiswa->get();
		
		$this->load->view('mahasiswa/data', $data);
	}

	public function add()
	{
		$data['title']= "Tambah Mahasiswa";

		$this->load->library('form_validation');

		if ($this->input->method() === 'post') {
			$this->load->model('m_mahasiswa');

			$rules = $this->m_mahasiswa->rules();

			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run() == FALSE) {
			return $this->load->view('mahasiswa/add', $data);
			}

			$mhs = [
				'id' => '',
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'hp' => $this->input->post('hp'),
				'alamat' => $this->input->post('alamat')
			];

			$insert_mhs = $this->m_mahasiswa->insert($mhs);

			if ($insert_mhs) {
				redirect('mahasiswa');
			}
		}
		else{
			$this->load->view('mahasiswa/add', $data);
		}
		
	}
	public function edit($id)
	{
		if ($this->input->method() === 'post') {

			$mhs = [
				'id' => $id,
				'nim' => $this->input->post('nim'),
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'hp' => $this->input->post('hp'),
				'alamat' => $this->input->post('alamat')
			];
			$updated = $this->m_mahasiswa->update($mhs);
			if ($updated) {
				$this->session->set_flashdata('message', 'Mahaiswa berhasil diubah');
				redirect('mahasiswa');
			}
		}
		else {
			$data['mahasiswa'] = $this->m_mahasiswa->find($id);
			$this->load->view('mahasiswa/edit', $data);
		}
		
	}
    public function delete($id)
    {
		// if (!$id) {
		// 	show_404();
		// }

		$deleted = $this->m_mahasiswa->delete($id);
		if ($deleted) {
			$this->session->set_flashdata('message', 'data mahasiswa terhapus');
			redirect('mahasiswa');
		}
    }
}
